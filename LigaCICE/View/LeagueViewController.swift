//
//  LeagueViewController.swift
//  LigaCICE
//
//  Created by formador on 6/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class LeagueViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var league = DataManager.shared.league
    var teamSelected: Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "LeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "teamCellIdentifier")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        league = DataManager.shared.league
        tableView.reloadData()
    }
    
}


extension LeagueViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return league.teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCellIdentifier", for: indexPath) as? LeagueTableViewCell
        
        cell?.nameLb.text = league.teams[indexPath.row].name
        
        cell?.contentView.backgroundColor = UIColor.lightGray
        
        return cell ?? UITableViewCell()
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "newTeamSegueIdentifier" {
            
            let teamViewcontroller = segue.destination as? TeamViewController
            
            let newTeam = Team(key: Date().description)
            
            league.teams.append(newTeam)
            
            DataManager.shared.league = league
            
            teamViewcontroller?.teamKey = newTeam.key
            
        } else if segue.identifier == "selectedTeamSegueIdentifier" {
            
            let teamViewcontroller = segue.destination as? TeamViewController

            teamViewcontroller?.teamKey = teamSelected?.key
        }
    }
    
}


extension LeagueViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        teamSelected = league.teams[indexPath.row]
        
        performSegue(withIdentifier: "selectedTeamSegueIdentifier", sender: self)
    }
}
