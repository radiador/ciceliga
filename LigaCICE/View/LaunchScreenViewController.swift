//
//  LaunchScreenViewController.swift
//  LigaCICE
//
//  Created by formador on 6/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    @IBOutlet weak var titleLb: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1, animations: {
            
            self.titleLb.frame.origin = CGPoint(x: self.titleLb.frame.origin.x, y: self.view.bounds.height + 30)
            
        }) { (true) in
            
            self.performSegue(withIdentifier: "launchFirstViewControllerIdentifier", sender: nil)
        }
            
    }

}
