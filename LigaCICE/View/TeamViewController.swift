//
//  TeamViewController.swift
//  LigaCICE
//
//  Created by formador on 6/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class TeamViewController: UIViewController {
    
    @IBOutlet weak var tableviewPlayers: UITableView!
    @IBOutlet weak var teamNameTxt: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    var league = DataManager.shared.league
    var teamKey: String? {
        get { return team?.key }
        set(key){
            team = league.teams.first(where: { (team) -> Bool in
                team.key == key
            })
        }
    }
    
    private var team: Team?
    
    var playerSelected: Player?

    override func viewDidLoad() {
        super.viewDidLoad()

        tableviewPlayers.register(UITableViewCell.self, forCellReuseIdentifier: "cellPlayersIdentifier")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        league = DataManager.shared.league
        
        teamNameTxt.text = team?.name

        tableviewPlayers.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Forzamos que el textfield termine la edición
        view.endEditing(true)

        updateLeague()
    }

}

extension TeamViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return team?.players.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPlayersIdentifier", for: indexPath)
        
        let player = team?.players[indexPath.row]
        
        cell.textLabel?.text = player?.name
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "newPlayerSegueIndentifier" {
            
            let playerTapViewcontroller = segue.destination as? PlayerTapViewController
            
            let newPlayer = Player()
            
            team?.players.append(newPlayer)
            
            playerTapViewcontroller?.player = newPlayer
            
            tableviewPlayers.reloadData()
            
        } else if segue.identifier == "selectedPlayerSegueIdentifier" {
            
            let playerTapViewcontroller = segue.destination as? PlayerTapViewController

            playerTapViewcontroller?.player = playerSelected
        }
    }
    
    private func updateLeague() {
        
        league.teams = league.teams.map({ (teamTmp) -> Team in
            
            return teamTmp.key == teamKey ? team! : teamTmp
        })
        
        DataManager.shared.league = league
    }
}


extension TeamViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        playerSelected = team?.players[indexPath.row]
        
        performSegue(withIdentifier: "selectedPlayerSegueIdentifier", sender: self)
    }
}


extension TeamViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        team?.name = textField.text ?? ""
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Ocultamos el teclado si esta presente
        return view.endEditing(true)
    }
}

