//
//  PlayerTapViewController.swift
//  LigaCICE
//
//  Created by formador on 28/2/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class PlayerTapViewController: UIViewController {

    @IBOutlet weak var positionsTb: UITableView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField! {
        didSet { ageTxt?.addDoneToolbar() }
    }
    @IBOutlet weak var driblingTxt: UITextField! {
        didSet { driblingTxt?.addDoneToolbar() }
    }
    @IBOutlet weak var speedTxt: UITextField! {
        didSet { speedTxt?.addDoneToolbar() }
    }
    
    var player: Player?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        positionsTb.register(UITableViewCell.self, forCellReuseIdentifier: "TpositionCellIdentifier")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        nameTxt.text = player?.name
        if let age = player?.age {
            ageTxt.text = "\(age)" //Forma habitual para convertir a String
        }
        if let drible = player?.drible {
            driblingTxt.text = String(drible) //Otra forma es con un cast normal
        }
        if let speed = player?.speed {
            speedTxt.text = String(speed)
        }
    }
}


extension PlayerTapViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Position.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TpositionCellIdentifier")
        
        let positionSelected = Position.allCases[indexPath.row]

        cell?.textLabel?.text = positionSelected.rawValue
        
        if player?.position == positionSelected {
            cell?.accessoryType = .checkmark
        } else {
            cell?.accessoryType = .none
        }
        
        return cell ?? UITableViewCell()
    }
    
}

extension PlayerTapViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let positionSelected = Position.allCases[indexPath.row]
        
        player?.position = positionSelected
        
        tableView.reloadData()
    }
}

extension PlayerTapViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if textField === nameTxt {
            player?.name = textField.text ?? ""
        } else if textField === ageTxt {
            player?.age = Int(textField.text ?? "")
        } else if textField === driblingTxt {
            player?.drible = Int(textField.text ?? "")
        } else if textField === speedTxt {
            player?.speed = Int(textField.text ?? "")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Ocultamos el teclado si esta presente
        return view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField !== nameTxt && !string.isEmpty {
            return string.isNumber
        }
        
        return true
    }
}

extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

extension UITextField {
    
    func addDoneToolbar(onDone: (target: Any, action: Selector)? = nil) {
        
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
}


