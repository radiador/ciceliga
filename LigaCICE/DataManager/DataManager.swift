//
//  DataManager.swift
//  LigaCICE
//
//  Created by formador on 6/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

class DataManager {
    
    //Singleton
    static let shared: DataManager = DataManager()
    
    private init() { }
    
    private var _league: League?
    
    var league: League {
        
        get {
            if _league == nil {
                _league = loadLeague()
            }
        
            return _league ?? League()
        }
        
        set(league) {
            _league = league
            saveLeague(league)
        }
    }
    
    static let leagueKey = "leagueKey"
    
    private func saveLeague(_ league: League) {

        //Codificar a JSON

        let encoder = JSONEncoder()

        encoder.outputFormatting = .prettyPrinted

        if let leagueEncoded = try? encoder.encode(league) {

            let prettyPrintedJson = String(data: leagueEncoded, encoding: .utf8 )

            if let prettyPrintedJson = prettyPrintedJson {
                print(prettyPrintedJson)
            }

            UserDefaults.standard.set(leagueEncoded, forKey: DataManager.leagueKey)
        }        
    }
    
    private func loadLeague() -> League? {
        
        let decoder = JSONDecoder()
        
        let leagueSaved = UserDefaults.standard.object(forKey: DataManager.leagueKey)
        
        if let leagueSavedData = leagueSaved as? Data {
            
            return try? decoder.decode(League.self, from: leagueSavedData)
        }
        
        return nil
    }
    
}
