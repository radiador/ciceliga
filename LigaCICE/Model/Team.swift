//
//  Team.swift
//  LigaCICE
//
//  Created by formador on 6/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation


struct Team: Codable {
    
    let key: String
    var name: String?
    var players = [Player]()
    
    init(key: String) {
        
        self.key = key
    }
}
