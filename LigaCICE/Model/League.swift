//
//  League.swift
//  LigaCICE
//
//  Created by formador on 6/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation


struct League: Codable {

    var name = "Liga CICE"
    var teams = [Team]()
}
