//
//  Player.swift
//  LigaCICE
//
//  Created by formador on 28/2/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

enum Position: String, CaseIterable, Codable {
    
    case forward
    case defensive
    case midfielder
    
}

struct Player: Codable {
    
    var name: String?
    var age: Int?
    var speed: Int?
    var drible: Int?
    var position: Position?

}

